import java.util.ArrayList;

public class Phonebook {

    ArrayList<Contact> contacts = new ArrayList<>();

    public Phonebook() {
    }

    public void getContacts() {
        if (contacts.isEmpty()) {
            System.out.println("The phonebook is currently empty.");
        } else {
        for (Contact contact : contacts) {
            System.out.println("------------------------");

            System.out.println(contact.getName() + " has the following registered number: " + "\n" + contact.getContactNumber());

            System.out.println(contact.getName() + " has the following registered address: " + "\n" + "my home in " + contact.getAddress());

            }
        }

     }


    public void setContact(String name, String contactNumber, String address) {
        Contact contact = new Contact(name, contactNumber, address);

        contacts.add(contact);
    }
}

